#include<TTree.h>
#include<iostream>
#include<TMath.h>
#include<TLorentzVector.h>

 //configuration
	//angular cuts
		bool angular=1;
	//lepton veto
		bool veto=1;
	//Wgamma control region
		bool CR=0;
	//e+MET control region
		bool CR2=0;
	//photon+jet control region
		bool CR3=0;
	//Exclusive
		bool nojets=0;
	// fake-rate forward region
		// double fr_forw=0.038; //full 2015+2016 new
		// double fr_forw=0.044; //full 2015+2016
		// double fr_forw=0.047; //13.2 fb-1
		// double fr_forw=0.0483; //3.2 fb-1
		//pT<250 GeV
		double fr_forw_le=0.045; double d_forw_le=0.003;
		// double fr_forw_le=0.0108; double d_forw_le=0.0009;
		//pT>250 GeV
		double fr_forw_he=0.032; double d_forw_he=0.007;
		// double fr_forw_he=0.0093; double d_forw_he=0.0012;
		// //pT<250 GeV
		// double fr_forw_le=0.038; double d_forw_le=0.002;
		// //pT>250 GeV
		// double fr_forw_he=0.038; double d_forw_he=0.002;
	// fake-rate central region
		// double fr_cent=0.02087; //full 2015+2016 new
		// double fr_cent=0.02319; //full 2015+2016
		// double fr_cent=0.02166; //13.2 fb-1
		// double fr_cent=0.0195; //3.2 fb-1
		//pT<250 GeV
		double fr_cent_le=0.0266; double d_cent_le=0.0011;
		// double fr_cent_le=0.0038; double d_cent_le=0.0003;
		//pT>250 GeV
		double fr_cent_he=0.014; double d_cent_he=0.002;
		// double fr_cent_he=0.0035; double d_cent_he=0.0003;
		// //pT<250 GeV
		// double fr_cent_le=0.02087; double d_cent_le=0.00092;
		// //pT>250 GeV
		// double fr_cent_he=0.02087; double d_cent_he=0.00092;
double forw_le=0; double forw_he=0; double cent_le=0; double cent_he=0;
double forw_le_sq=0; double forw_he_sq=0; double cent_le_sq=0; double cent_he_sq=0;

	// dPhi (MET, jet)
		// double metjet=0.5;
		double metjet=0.4;
	// dPhi (MET, photon)
		// double metgam=0.6;
		double metgam=2.1;

// bool ph_tight, loosePrime3, loosePrime4;
  unsigned int n_ph, n_e_looseBL, n_e_medium, n_mu, n_jet, N_f_l, N_f_h, N_c_l, N_c_h;
  double ph_pt, ph_eta, ph_phi, ph_iso_et, ph_iso_pt, jet_pt, jet_eta, jet_phi, jet_E, subjet_pt, subjet_eta, subjet_phi, subjet_E, metTST_pt, metTST_phi,\
   e_lead_pt, e_lead_eta, e_lead_phi, e_lead_E, weight, ph_zp;
   unsigned int ph_isem;
   int mc_ph_type;
  // double ph_px, ph_py, ph_pz, ph_E, ph_iso_et, ph_iso_pt, jet_px, jet_py, jet_pz, jet_E, metTST_px, metTST_py, metTST_pz, metTST_E,\
  //  e_lead_px, e_lead_py, e_lead_pz, e_lead_E, weight;

  TLorentzVector met, ph, jet, subjet, el;
// double lumi_for_calc = 3.795e+04; //nb-1
// double lumi_for_calc = 3212.96e+3; //nb-1
// double lumi_for_calc = 13212.96e+3; //nb-1
double lumi2015 = 3212.96e+3; //nb-1
double lumi2016 = 32861.6e+3; //nb-1
double lumi_for_calc = lumi2016 + lumi2015;
// double koef_sig;
// Int_t i;
double max=0;
TFile *f = 0;
double err, events;
Long64_t N=0;
double Sum=0;
double SumA=0; double SumB=0; double SumC=0; double SumD=0;
double SumAsq=0; double SumBsq=0; double SumCsq=0; double SumDsq=0;
double SumAnom=0; double SumBnom=0; double SumCnom=0; double SumDnom=0;
double Sum_bar=0; Double_t w_bar=0;
double Sum_end=0; Double_t w_end=0;
